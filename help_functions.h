#ifndef HELP_FUNCTIONS_H
#define HELP_FUNCTIONS_H

#include <iostream>
#include <string>
#include <vector>
#include "hashtable.h"

#define MAX_R 100
#define TIMES 100

using namespace std;

char* get_arguments(const char*[], int, const char*, bool = false);
void memory_alloc_for_args(char**, char **, char**, char**, char**, char**);
void free_for_args(char*, char*, char*, char*, char*, char*); 
void read_from_input(char**, char**, char**, char**, char**);
void perror_exit(const char*);
double dot_product(const vector<double>&, const vector<double>&);
double uniform_distribution(int, int);
double normal_distribution(double, double);
void insert_curves_into_hashtables(vector<HashTable>&, int, double, int, const char*);
void search_curves_from_hashtables(const vector<HashTable>&, int, double, int, double, const char*, const char* , vector<Curve>&, vector<vector<Curve> >&, const vector<bool>&, vector<double>&, vector<double>&, vector<double>&, bool = true);
void general_search(const vector<HashTable>&, int, double, int, double, const char*, const char*, vector<Curve>&, vector<vector<Curve> >&, vector<double>&, vector<double>&, vector<double>&, vector<bool>&); 
int convert_string_to_int(const char*);

#endif
