CC = g++
FLAGS = -g -c
OUT = run
OBJS = main.o help_functions.o hash_functions.o distances_functions.o file_functions.o list.o hashtable.o curve.o

run: $(OBJS)
	$(CC) -g $(OBJS) -o $(OUT)

%.o: %.cpp %.h
	$(CC) $(FLAGS) $< -o $@

clean:
	rm -f $(OBJS) $(OUT) 
