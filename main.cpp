#include <iostream>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <string>
#include <vector>
#include <ctime>
#include "help_functions.h"
#include "hash_functions.h"
#include "file_functions.h"
#include "distances_functions.h"
#include "curve.h"
#include "hashtable.h"

using namespace std;

int main(int argc, const char *argv[]) {
    srand(time(NULL));
    
    double delta = 0.2;
    int k = 2, L = 3;
    
    char *dist_function = get_arguments(argv, argc, "-function");
    char *hash_function = get_arguments(argv, argc, "-hash");
    char *k_str = get_arguments(argv, argc, "-k");
    char *L_str = get_arguments(argv, argc, "-L");
    char *input_file = get_arguments(argv, argc, "-d");
    char *query_file = get_arguments(argv, argc, "-q");
    char *output_file = get_arguments(argv, argc, "-o");
    char *stats = get_arguments(argv, argc, "-stats", true);
    
    memory_alloc_for_args(&dist_function, &hash_function, &input_file, &query_file, &output_file, &stats); 
    
    if (strlen(k_str)) {
        k = convert_string_to_int(k_str);
    }
    
    if (strlen(L_str)) {
        L = convert_string_to_int(L_str);
    }

    clock_t start_timer;
    double R;
    int table_size, max_points = 0, min_points = -1;
    int dim = 2, value;
    string choice;

    read_from_input(&input_file, &dist_function, &hash_function, &query_file, &output_file);
    input_curves = read_file(input_file, dim); 

    for (int i = 0; i < input_curves.size(); ++i) {
        value = input_curves[i].get_length();
        max_points = max(max_points, value);
        min_points = (min_points == -1 ? value : min(min_points, value));
    }
    
    for (int i = 0; i < max_points * k * dim; ++i) {
        vec_r.push_back(rand() % MAX_R);
    } 
    
    // Create L hashTables
    table_size = (int)input_curves.size() / 4 + 1; 
    vector<HashTable> hashtables(L + 1, HashTable(table_size)); 
    
    for (int i = 0; i < L; ++i) {
        hashtables[i].set_id(i);
    }
    
    insert_curves_into_hashtables(hashtables, L, delta, k, hash_function); // Insert input_curves into hashtables  
    
    do {
        search_curves = read_file(query_file, dim, R, true);

        vector<double> min_duration(search_curves.size(), -1);
        vector<double> max_duration(search_curves.size());
        vector<double> sum_duration(search_curves.size());
        vector<double> true_duration(search_curves.size());

        if (strlen(stats)) { // stats flag is given
            vector<double> minim(search_curves.size(), -1);
            vector<double> maxim(search_curves.size());
            vector<double> sum(search_curves.size());

            for (int times = 0; times < TIMES; ++times) {
                hashtables.clear(); // Clear everything
                t_shift.clear();
                vec_line.clear();
                
                for (int i = 0; i < L; ++i) {
                    HashTable table(table_size);
                    hashtables.push_back(table);
                }

                for (int i = 0; i < L; ++i) {
                    hashtables[i].set_id(i);
                }
                
                insert_curves_into_hashtables(hashtables, L, delta, k, hash_function);

                vector<Curve> closest_curve(search_curves.size(), Curve());
                vector<vector<Curve> > R_closest_curves(search_curves.size());
                vector<bool> grid_curves_found(search_curves.size(), false);

                general_search(hashtables, L, delta, k, 0, hash_function, dist_function, closest_curve, R_closest_curves, 
                               max_duration, min_duration, sum_duration, grid_curves_found);

                for (int i = 0; i < search_curves.size(); ++i) { // keep statistics for distances
                    double dist = compute_distance(search_curves[i], closest_curve[i], dist_function);
                    
                    minim[i] = (minim[i] == -1 ? dist : min(minim[i], dist));
                    maxim[i] = max(maxim[i], dist);
                    sum[i] += dist;
                }
            }
            
            vector<Curve> real_closest_curve(search_curves.size(), Curve());
            
            for (int i = 0; i < search_curves.size(); ++i) { // run bruteforce
                start_timer = clock();
                real_closest_curve[i] = brute_force_closest_curve(search_curves[i], dist_function);
                true_duration[i] = (clock() - start_timer) / (double)CLOCKS_PER_SEC;
            }
            
            print_file_stats(output_file, dist_function, hash_function, minim, maxim, sum, real_closest_curve, max_duration, 
                             min_duration, sum_duration, true_duration);
            break; 
        }
        
        vector<Curve> closest_curve(search_curves.size(), Curve());
        vector<vector<Curve> > R_closest_curves(search_curves.size());
        vector<Curve> real_closest_curve(search_curves.size(), Curve());
        vector<bool> grid_curves_found(search_curves.size(), false);
        
        general_search(hashtables, L, delta, k, R, hash_function, dist_function, closest_curve, R_closest_curves, 
                       max_duration, min_duration, sum_duration, grid_curves_found); // implement search method
        
        for (int i = 0; i < search_curves.size(); ++i) { // run bruteforce
            real_closest_curve[i] = brute_force_closest_curve(search_curves[i], dist_function);
        }
        
        print_file(output_file, closest_curve, R_closest_curves, real_closest_curve, dist_function, hash_function, R, grid_curves_found);
    
        cout << "Enter 't' if you want to terminate the program, otherwise insert a search file: ";
        cin >> choice;
        
        if (choice == "t") {
            break;
        } else {
            free(query_file);
            query_file = (char*)"";

            query_file = strdup(choice.c_str());
        }

    } while(1);
    
    free_for_args(dist_function, hash_function, input_file, query_file, output_file, stats);
    return 0;
}
