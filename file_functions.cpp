#include <iostream>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <string>
#include <vector>
#include <string>
#include <algorithm>
#include "file_functions.h"
#include "help_functions.h"
#include "distances_functions.h"

using namespace std;

vector<Curve> read_file(const char *file_name, int &dim, double &R, bool search) {
    vector<Curve> curves;
    vector<double> point;
    ifstream file(file_name);
    string str, id;
    int num_points;
    double val;
    char chr;
    bool read_id = false;
    
    file >> str;

    if (str == "dimension:" || str == "R:") {
        file >> val;
        
        if (!search) {
            dim = (int)val;
        } else {
            R = val;
        }
    } else {
        read_id = true;
        id = str;
    }

    while (1) {
        if (!read_id) {
            file >> id;
        } else {
            read_id = false;
        }
         
        if (file.eof()) {
            break;
        }
        
        file >> num_points;
        Curve curve(id, dim);

	    for (int i = 0; i < num_points; i++) {
            point.clear();

            for(int j = 0; j < dim; ++j) {
                file >> chr;
                file >> val;
                point.push_back(val);
            }
            
            file >> chr;
            
            if (i < num_points - 1) {
                file >> chr;
            }

            if (!curve.is_empty() && curve.get_last_point() == point) { // remove duplicates
                continue;
            }

            curve.insert_point(point);
        }
        
        curves.push_back(curve);
    }
    
    file.close();
    return curves;
}

void print_file(const char *file_name, const vector<Curve> &closest_curve, vector<vector<Curve> > &R_closest_curves, const vector<Curve> &real_closest_curve, const char *dist_function, const char *hash_function, double R, const vector<bool> &grid_curves_found) {
    ofstream out_file(file_name);
    
    for (int i = 0; i < search_curves.size(); ++i) {
        double real_dist = compute_distance(search_curves[i], real_closest_curve[i], dist_function);
        
        out_file << "Query: " << search_curves[i].get_id() << endl;
        out_file << "DistanceFunction: " << dist_function << endl;
        out_file << "HashFunction: " << hash_function << endl;
        out_file << "FoundGridCurve: " << (grid_curves_found[i] ? "True" : "False") << endl; 
        
        if (!R) {
            double lsh_dist = compute_distance(search_curves[i], closest_curve[i], dist_function);
            
            out_file << "LSH Nearest neighbor: " << closest_curve[i].get_id() << endl;
            out_file << "True Nearest neighbor: " << real_closest_curve[i].get_id() << endl;
            out_file << "distanceLSH: " << lsh_dist << endl;
            out_file << "distanceTrue: " << real_dist << endl;
        } else { 
            double min_lsh_dist = -1, temp_dist;
            Curve min_lsh_curve;
            
            for (int j = 0; j < R_closest_curves[i].size(); ++j) {
                temp_dist = compute_distance(search_curves[i], R_closest_curves[i][j], dist_function);
                
                if (min_lsh_dist == -1 || temp_dist < min_lsh_dist) {
                    min_lsh_dist = temp_dist;
                    min_lsh_curve = R_closest_curves[i][j];
                }
            }
            
            out_file << "LSH Nearest neighbor: " << min_lsh_curve.get_id() << endl;
            out_file << "True Nearest neighbor: " << real_closest_curve[i].get_id() << endl;
            out_file << "distanceLSH: " << min_lsh_dist << endl;
            out_file << "distanceTrue: " << real_dist << endl;
            out_file << "R-near neighbors:" << endl;
            
            sort(R_closest_curves[i].begin(), R_closest_curves[i].end());
            
            for (int j = 0; j < R_closest_curves[i].size(); ++j) {
                if (!j || R_closest_curves[i][j].get_id() != R_closest_curves[i][j-1].get_id()) {
                    out_file << R_closest_curves[i][j].get_id() << endl;
                }
            }
        }

        out_file << endl;
    }
    
    out_file.close();
}

void print_file_stats(const char *file_name, const char *dist_function, const char *hash_function, const vector<double> &minim, const vector<double> &maxim, const vector<double> &sum, const vector<Curve> &real_closest_curve, const vector<double> &max_duration, const vector<double> &min_duration, const vector<double> &sum_duration, const vector<double> &true_duration) {
    ofstream out_file(file_name);
    
    for (int i = 0; i < search_curves.size(); ++i) {
        out_file << "Query: " << search_curves[i].get_id() << endl;
        out_file << "DistanceFunction: " << dist_function << endl;
        out_file << "HashFunction: " << hash_function << endl;

        double real_dist = compute_distance(search_curves[i], real_closest_curve[i], dist_function);
        
        double value = minim[i] - real_dist;
        out_file << "|minDistance - distanceTrue|: " << value << endl;
        
        value = maxim[i] - real_dist;
        out_file << "|maxDistance - distanceTrue|: " << value << endl;
        
        value = (sum[i] / (double)TIMES) - real_dist;
        out_file << "|avgDistance - distanceTrue|: " << value << endl;
        
        value = min_duration[i];
        out_file << "tLSHmin: " << value << endl;
        
        value = max_duration[i];
        out_file << "tLSHmax: " << value << endl;
        
        value = sum_duration[i] / (double)TIMES;
        out_file << "tLSHavg: " << value << endl;
        
        value = true_duration[i];
        out_file << "tTrue: " << value << endl;
        
        out_file << endl;
    }
    
    out_file.close();
}
