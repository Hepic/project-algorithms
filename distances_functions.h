#ifndef DISTANCES_H
#define DISTANCES_H

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <vector>
#include "curve.h"

using namespace std;

vector<double> find_closest_point(const vector<double>&, const vector<double>&, double);
double euclidean_distance(const vector<double>&, const vector<double>&);
double discrete_frechet_distance(const Curve&, const Curve&);
double dynamic_time_wrapping(const Curve&, const Curve&);
Curve brute_force_closest_curve(const Curve&, const char*);
double compute_distance(const Curve&, const Curve&, const char*);

#endif
