#ifndef FILE_FUNCTIONS_H
#define FILE_FUNCTIONS_H

#include <vector>
#include <string>
#include "curve.h"

using namespace std;

static double Null = 0;

vector<Curve> read_file(const char*, int&, double& = Null, bool = false);
void print_file(const char*, const vector<Curve>&, vector<vector<Curve> >&, const vector<Curve>&, const char*, const char*, double, const vector<bool>&);
void print_file_stats(const char*, const char*, const char*, const vector<double>&, const vector<double>&, const vector<double>&, const vector<Curve>&, const vector<double>&, const vector<double>&, const vector<double>&, const vector<double>&);

#endif
