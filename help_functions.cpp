#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <math.h>
#include <algorithm>
#include "help_functions.h"
#include "hash_functions.h"
#include "distances_functions.h"

char *get_arguments(const char *argv[], int len, const char *flag, bool same) {
    for (int i = 0; i < len; ++i) {
        if(!strcmp(argv[i], flag)) {
            if (same) {
                return (char*)argv[i];
            } else {
                return (char*)argv[i+1];
            }
        }
    }

    return (char*)"";
}

void memory_alloc_for_args(char **dist_function, char **hash_function, char **input_file, char **query_file, char **output_file, char **stats) { 
    if (strlen(*dist_function)) {
        *dist_function = strdup(*dist_function);
    }
    
    if (strlen(*hash_function)) {
        *hash_function = strdup(*hash_function);
    }
    
    if (strlen(*input_file)) {
        *input_file = strdup(*input_file);
    }
    
    if (strlen(*query_file)) {
        *query_file = strdup(*query_file);
    }
    
    if (strlen(*output_file)) {
        *output_file = strdup(*output_file);
    }
    
    if (strlen(*stats)) {
        *stats = strdup(*stats);
    }
}

void free_for_args(char *dist_function, char *hash_function, char *input_file, char *query_file, char *output_file, char *stats) { 
    if (strlen(dist_function)) {
        free(dist_function);
    }
    
    if (strlen(hash_function)) {
        free(hash_function);
    }
    
    if (strlen(input_file)) {
        free(input_file);
    }
    
    if (strlen(query_file)) {
        free(query_file);
    }
    
    if (strlen(output_file)) {
        free(output_file);
    }
    
    if (strlen(stats)) {
        free(stats);
    }
}

void read_from_input(char **input_file, char **dist_function, char **hash_function, char **query_file, char **output_file) {
    if (!strlen(*input_file)) {
        printf("Input file: ");
        *input_file = (char*)malloc(sizeof(char) * 128);
        scanf("%s", *input_file);
    }

    if (!strlen(*dist_function)) {
        printf("Distance Function: ");
        *dist_function = (char*)malloc(sizeof(char) * 128);
        scanf("%s", *dist_function);
    }

    if (!strlen(*hash_function)) {
        printf("Hash Function: ");
        *hash_function = (char*)malloc(sizeof(char) * 128);
        scanf("%s", *hash_function);
    }

    if (!strlen(*query_file)) {
        printf("Query File: ");
        *query_file = (char*)malloc(sizeof(char) * 128);
        scanf("%s", *query_file);
    }
    
    if (!strlen(*output_file)) {
        printf("Output File: ");
        *output_file = (char*)malloc(sizeof(char) * 128);
        scanf("%s", *output_file);
    }
}

void perror_exit(const char *msg) {
    perror(msg);
    exit(EXIT_FAILURE);
}

double dot_product(const vector<double> &vec_1, const vector<double> &vec_2) {
    double dot = 0;

    for (int i = 0; i < vec_1.size(); ++i) {
        dot += vec_1[i] * vec_2[i];
    }

    return dot;
}

double uniform_distribution(int A, int B) {
    double zero_to_one = (double)rand() / ((double)RAND_MAX + 1.0);
    double ret = A + zero_to_one * (B - A);

    return ret;
}

double normal_distribution(double mean, double stddev) {
    double U, V, S;
    static double X, Y;
    static bool Y_time = false;
    
    if(Y_time)
    {
        Y_time = false;
        return (Y * stddev + mean);
    }
    
    do {
        U = uniform_distribution(-1, 1);
        V = uniform_distribution(-1, 1);
        S = U*U + V*V;
    } while (S >= 1.0);

    X = U * sqrt((-2.0 * log(S)) / S);
    Y = V * sqrt((-2.0 * log(S)) / S);
    Y_time = true;

    return (X*stddev + mean);
}

void insert_curves_into_hashtables(vector<HashTable> &hashtables, int L, double delta, int k, const char *hash_function) {
    for (int i = 0; i < L; ++i) { // Insert curves in hashtable
        vector<Curve> concat_curves = multiple_grids(delta, k);
        
        for (int j = 0; j < concat_curves.size(); ++j) {
            hashtables[i].insert(input_curves[j], concat_curves[j], hash_function);
        }
    }
}

void search_curves_from_hashtables(const vector<HashTable> &hashtables, int L, double delta, int k, double R, const char *hash_function, const char *dist_function, vector<Curve> &closest_curve, vector<vector<Curve> > &R_closest_curves, const vector<bool> &grid_curves_found, vector<double> &max_duration, vector<double> &min_duration, vector<double> &sum_duration, bool check) {
    vector<double> min_curve_dist(search_curves.size(), -1);
    double dist, duration;
    clock_t start_timer;
    
    for (int i = 0; i < search_curves.size(); ++i) {
        if (grid_curves_found[i] && R == 0) {
            continue;
        }
        
        start_timer = clock();
        
        for (int j = 0; j < L; ++j) {
            Curve concat_curve = multiple_grids_curve(delta, k, hashtables[j].get_id(), search_curves[i]); // retrieve concatenated grid_curve 

            vector<Curve> closer_curves;
            closer_curves = hashtables[j].search(search_curves[i], concat_curve, hash_function, dist_function, R, check);
            
            if (!closer_curves.empty()) {
                if (!R) {
                    dist = compute_distance(search_curves[i], closer_curves.front(), dist_function);
                    
                    if (min_curve_dist[i] == -1 || dist < min_curve_dist[i]) {
                        closest_curve[i] = closer_curves.front();
                        min_curve_dist[i] = dist;
                    }
                } else {
                    for (int k = 0; k < closer_curves.size(); ++k) {
                        R_closest_curves[i].push_back(closer_curves[k]);
                    }
                }
            }
        }
        
        duration = (clock() - start_timer) / (double)CLOCKS_PER_SEC;
        
        min_duration[i] = (min_duration[i] == -1 ? duration : min(min_duration[i], duration));
        max_duration[i] = max(max_duration[i], duration);
        sum_duration[i] += duration;
    }
}

void general_search(const vector<HashTable> &hashtables, int L, double delta, int k, double R, const char *hash_function, const char *dist_function, vector<Curve> &closest_curve, vector<vector<Curve> > &R_closest_curves, vector<double> &max_duration, vector<double> &min_duration, vector<double> &sum_duration, vector<bool> &grid_curves_found) {
    search_curves_from_hashtables(hashtables, L, delta, k, R, hash_function, dist_function, 
                                  closest_curve, R_closest_curves, grid_curves_found, max_duration, 
                                  min_duration, sum_duration); // check first if grid_curve is same
    
    for (int i = 0; i < search_curves.size(); ++i) { // keep curves that grid_curve was found
        if (!R) {
            if (!closest_curve[i].get_id().empty()) {
                grid_curves_found[i] = true;
            }
        } else {
            if (!R_closest_curves[i].empty()) {
                grid_curves_found[i] = true;
            }
        }
    }
    
    search_curves_from_hashtables(hashtables, L, delta, k, R, hash_function, dist_function, 
                                  closest_curve, R_closest_curves, grid_curves_found, max_duration, 
                                  min_duration, sum_duration, false); // search without checking grid_curve for curves that we did not find anything before
}

int convert_string_to_int(const char *str) {
    int ret = 0;

    while (*str != '\0') {
        ret = ret * 10 + *str - '0';
        ++str;
    }

    return ret;
}
